@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">ثبت ملک</div>

                    <div class="card-body">

                        <form method="POST" action="my/addproperty">
                            @csrf
                            <input name="name" placeholder="نام">
                            <select  name="type">
                                <option value="1">آپارتمان</option>
                                <option value="2">ویلایی</option>
                                <option value="3">باغ</option>
                            </select>
                            <input name="description" placeholder="توضیحات">
                            <select  name="cars">
                                @foreach($areas as $area)
                                <option value={{$area["id"]}}>{{$area["name"]}}</option>
                                @endforeach
                            </select>
                            <input name="image" type="image">
                            <button type="submit">ثبت</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

