@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">املاک</div>

                    <div class="card-body">
                        @foreach($properties as $property)
                            <div class="row property">
                                <div style="width: 100px;text-align: center" class="m-2" >{{$property["name"]}}</div>

                                <div style="width: 100px;text-align: center" class="m-2">{{$property["type"]}}</div>

                                <div style="width: 100px;text-align: center" class="m-2">{{$property["area"]["name"]}}</div>

                                <div style="width: 100px;text-align: center" class="m-2">{{$property["description"]}}</div>

                            </div>
                            <div class="row">
                                @foreach($property["images"] as $image)
                                    <div class="m-2" style="width: 200px;height: 200px">

                                <img style="width: 100%;height: 100%"  src={{url("storage/".$image["url"])}}/>
                                    </div>
                                @endforeach
                            </div>
                        @if(!empty($like))
                            <div class="row">
                                <div class="like" data-id={{$property["id"]}}>
                                    <div style="border: 1px black solid">
                                        like
                                    </div>
                                </div>
                            </div>
                            @endif
                            @if(!empty($edit))
                            <div class="row">
                                <div class="edit" data-id={{$property["id"]}}>
                                    <div style="border: 1px black solid">
                                        edit
                                    </div>
                                </div>
                            </div>
                            @endif
                            @if(!empty($confirm))
                            <div class="row">
                                <div class="edit" data-id={{$property["id"]}}>
                                    <div style="border: 1px black solid">
                                        confirm
                                    </div>
                                </div>
                            </div>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

