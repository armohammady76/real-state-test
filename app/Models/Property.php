<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    protected $table="property";

    protected $fillable=["name","type","user_id","description","status","area_id"];

    protected $casts=["type"=>\App\Casts\PropertyTypeCast::class];

    public function Images ()
    {
        return $this->belongsToMany('App\Models\Image', 'property_image', 'property_id', 'image_id');
    }

    public function Area ()
    {
        return $this->hasOne("App\Models\Area","id","area_id");
    }

    public function User ()
    {
        return $this->hasOne("App\User","id","user_id");
    }


}
