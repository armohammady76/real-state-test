<?php

namespace App\Http\Controllers;

use App\Models\Property;
use Illuminate\Http\Request;

class PropertyController extends Controller
{

    public function Handler ()
    {
        $properties=Property::where("status",'=',1)->with("Images")->with("Area")->get()->toArray();

        return view("property",["properties"=>$properties,"like"=>TRUE]);

    }
}
