<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LikePropertyController extends Controller
{

    public function Handler ($id)
    {
        Auth::user()->Likes()->attach([$id]);
    }
}
