<?php

namespace App\Http\Controllers\My;

use App\Http\Controllers\Controller;
use App\Models\Property;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PropertyController extends Controller
{

    public function Handler ()
    {
        $properties=Auth::user()->Properties()->with("Images")->with("Area")->get()->toArray();

        return view("property",["properties"=>$properties,"edit"=>TRUE]);
    }
}
