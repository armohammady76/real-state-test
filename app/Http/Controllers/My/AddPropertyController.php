<?php

namespace App\Http\Controllers\My;

use App\Http\Controllers\Controller;
use App\Mail\AddPropertyMail;
use App\Models\Area;
use App\Models\Image;
use App\Models\Property;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class AddPropertyController extends Controller
{

    public function Handler ()
    {
        $areas=Area::get();

        return view("propertyRegister",["areas"=>$areas]);
    }

    public function Data (Request $request)
    {
        $this->validate($request,[
           "name"=>"required|string|max:45",
           "type"=>"required|numeric|max:3|min:1",
           "area"=>"required|numeric|exists:area,id",
           "description"=>"nullable|string|max:300",
        ]);
        $property= new Property([
            "name"        => $request->input("name"),
            "type"        => $request->input("type"),
            "status"      => 0,
            "description" => $request->input("description"),
            "area_id"     => $request->input("area"),
        ]);

        Auth::user()->Properties()->save($property);

        if(!empty($request->files()))
        {
            foreach ($request->files() as $image)
            {
                $image->store();

                $image=Image ::create([
                    "url"  => $image->path(),
                    "type" => $image->extension(),
                ]);

                $property->Images()->attach([$image->id]);
            }
        }
        Mail::to(Auth::user()->email)->send(new AddPropertyMail());
        return response("Success");
    }
}
