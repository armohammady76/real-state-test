<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Mail\ConfirmPropertyMail;
use App\Models\Property;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ConfirmPropertyController extends Controller
{

    public function Handler ()
    {
        $properties=Property::where("status",'=',0)->with("Images")->with("Area")->get()->toArray();

        return view("property",["properties"=>$properties,"confirm"=>TRUE]);
    }

    public function Data (Request $request)
    {
        $this->validate($request,[
            "id"=>"required|numeric|exists:property,id"
        ]);

        Property::where("id",'=',$request->input("id"))->updare(["status"=>1]);

        Mail::to(Property::find($request->input("id"))->User()->value("email"))->send(new ConfirmPropertyMail());

        return response("Success");
    }
}
