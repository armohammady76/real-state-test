<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class checkAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $roles=Auth::user()->Roles()->get()->toArray();

        $permission=FALSE;

        foreach ($roles as $role)
        {
            if($role["role"]==3)
            {
                $permission=TRUE;

                break;
            }
        }
        if ($permission)
        return $next($request);
        else
            die(403);
    }
}
