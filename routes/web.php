<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/property', 'PropertyController@Handler')->name('properties');
Route::get('/like/{id}', 'LikePropertyController@Handler')->name('properties')->middleware('auth');;

Route::prefix("my")->group(function ()
{
    Route::get('property', 'My\PropertyController@Handler')->name('user properties')->middleware('auth');;
    Route::get('addproperty', 'My\AddPropertyController@Handler')->name('user add properties')->middleware('auth');;
    Route::post('addproperty', 'My\AddPropertyController@Data')->name('user data add properties')->middleware('auth');;

});
Route::prefix("admin")->group(function ()
{

    Route::get('confirmproperty', 'Admin\ConfirmPropertyController@Handler')->name('admin Confirm properties')->middleware(['auth',"admin"]);;
    Route::post('confirmproperty', 'Admin\ConfirmPropertyController@Data')->name('admin data Confirm properties')->middleware(['auth',"admin"]);;

});
