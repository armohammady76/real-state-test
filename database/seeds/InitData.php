<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class InitData extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run ()
    {
        \App\Models\Property ::create([
            "id"          => 1,
            "name"        => "building 1 ",
            "type"        => 1,
            "status"      => 0,
            "user_id"     => 1,
            "description" => "first",
            "area_id"     => 1,
        ]);
        \App\Models\Property ::create([
            "id"          => 2,
            "name"        => "building 2 ",
            "type"        => 2,
            "status"      => 0,
            "user_id"     => 2,
            "description" => "second",
            "area_id"     => 2,
        ]);
        \App\Models\Property ::create([
            "id"          => 3,
            "name"        => "building 3 ",
            "type"        => 3,
            "status"      => 1,
            "user_id"     => 2,
            "description" => "third",
            "area_id"     => 3,
        ]);

        \App\Models\Image ::create([
            "id"   => 1,
            "url"  => "1.jfif",
            "type" => "jfif",
        ]);
        \App\Models\Image ::create([
            "id"   => 2,
            "url"  => "2.jfif",
            "type" => "jfif",
        ]);
        \App\Models\Image ::create([
            "id"   => 3,
            "url"  => "3.jfif",
            "type" => "jfif",
        ]);
        \App\Models\Image ::create([
            "id"   => 4,
            "url"  => "4.jfif",
            "type" => "jfif",
        ]);
        \App\Models\Image ::create([
            "id"   => 5,
            "url"  => "5.jfif",
            "type" => "jfif",
        ]);
        \App\Models\Image ::create([
            "id"   => 6,
            "url"  => "6.jfif",
            "type" => "jfif",
        ]);
        \App\Models\PropertyImage ::create([
            "id"          => 1,
            "property_id" => 1,
            "image_id"    => 1,
        ]);
        \App\Models\PropertyImage ::create([
            "id"          => 2,
            "property_id" => 1,
            "image_id"    => 2,
        ]);
        \App\Models\PropertyImage ::create([
            "id"          => 3,
            "property_id" => 2,
            "image_id"    => 3,
        ]);
        \App\Models\PropertyImage ::create([
            "id"          => 4,
            "property_id" => 2,
            "image_id"    => 4,
        ]);
        \App\Models\PropertyImage ::create([
            "id"          => 5,
            "property_id" => 3,
            "image_id"    => 5,
        ]);
        \App\Models\PropertyImage ::create([
            "id"          => 6,
            "property_id" => 3,
            "image_id"    => 6,
        ]);
        \App\Models\UserLike ::create([
            "id"          => 1,
            "property_id" => 2,
            "user_id"     => 1,
        ]);
        \App\Models\UserLike ::create([
            "id"          => 2,
            "property_id" => 3,
            "user_id"     => 1,
        ]);
        \App\Models\UserLike ::create([
            "id"          => 3,
            "property_id" => 2,
            "user_id"     => 2,
        ]);
        \App\User ::create([
            "id"       => 1,
            'name'     => 'ali',
            'email'    => 'ali@gmail.com',
            'password' => Hash ::make("123456"),
        ]);
        \App\User ::create([
            "id"       => 2,
            'name'     => 'reza',
            'email'    => 'reza@gmail.com',
            'password' => Hash ::make("123456"),
        ]);
        \App\Models\UserRole ::create([
            "id"      => 1,
            "user_id" => 1,
            "role"    => 3,
        ]);
        \App\Models\Area ::create([
            "id"   => 1,
            "name" => "Area 1",
        ]);
        \App\Models\Area ::create([
            "id"   => 2,
            "name" => "Area 2",
        ]);
        \App\Models\Area ::create([
            "id"   => 3,
            "name" => "Area 3",
        ]);
        \App\Models\Area ::create([
            "id"   => 4,
            "name" => "Area 4",
        ]);
    }
}
